// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.


const cacheFunction = require("../cacheFunction.cjs");


// Variable to store returned function by cacheFunction function 
let invokeCallbackFunction;


// Defining the cb function to be invoked n times 
function callbackFunction(...values) {
    return values;
}


// Assigning the return function by cacheFunction 
invokeCallbackFunction = cacheFunction(callbackFunction);

// Calling invokeCallback function  returned by cacheFunction
console.log(invokeCallbackFunction(2,2,3));

console.log(invokeCallbackFunction(3,4,5));

console.log(invokeCallbackFunction(2));