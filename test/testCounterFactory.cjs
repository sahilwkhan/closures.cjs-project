// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.


const counterFactory = require("../counterFactory.cjs");

// Variable to store returned object by counterFactory function 
let methodObjects;

// Variables for methods of object returned by counterFactory function 
let incrementOutput, decrementOutput;

methodObjects = counterFactory();

// Calling increment method from object returned by counterFactory function, to increase counterVariable by 1
incrementOutput = methodObjects.increment();  
console.log(incrementOutput);

// Calling decrement method from object returned by counterFactory function, to decrease counterVariable by 1
decrementOutput = methodObjects.decrement();  
console.log(decrementOutput);