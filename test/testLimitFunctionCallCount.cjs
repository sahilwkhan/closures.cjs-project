// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned


const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

// Variable to store returned function by limitFunctionCallCount function 
let invokesCallbackFunction;

// Defining the callbackFunction  to be invoked n times 
function callbackFunction(...values) {
    let total = 0
   for(let index = 0; index < values.length;index++){
       total += values[index];
   }
   return total;
}

// Variable to store the value representing how many times callbackFunction to be invoked
let invokeNumber = 3;

invokesCallbackFunction = limitFunctionCallCount(callbackFunction,invokeNumber);

// Calling invokeCallback function  returned by limitFunctionCallCount function

console.log(invokesCallbackFunction());
console.log(invokesCallbackFunction(1,2));
console.log(invokesCallbackFunction(1,2,3));
console.log(invokesCallbackFunction(1,2,3));
console.log(invokesCallbackFunction());