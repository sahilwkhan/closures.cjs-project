module.exports = counterFactory;

function counterFactory() {

    let testVariable = 0;
    
    function increment() {
        testVariable++;
        return testVariable;
    }

    function decrement() {
        testVariable--;
        return testVariable
    }
    return { increment, decrement };
}