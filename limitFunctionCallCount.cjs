module.exports = limitFunctionCallCount;

function limitFunctionCallCount(callbackFunction,invokeNumber) {

    if (typeof callbackFunction != 'function' || callbackFunction == undefined){
        throw new Error('callback function must be valid and cannot be empty.');
    }
    if (typeof invokeNumber != 'number' || invokeNumber == undefined){
        throw new Error('Value of invokeNumber must be an Integer and cannot be empty.');
    }


    function invokeCallback(...values) {
        if (invokeNumber > 0) {
            invokeNumber--;
            return callbackFunction(...values);
        }
        else {
            return null;
        }
    }
    
    return invokeCallback;
}
