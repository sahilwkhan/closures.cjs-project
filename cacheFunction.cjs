module.exports = cacheFunction;


function cacheFunction(callbackFunction) {

    if (typeof callbackFunction != 'function' || callbackFunction == undefined){
        throw new Error('callback function must be valid and cannot be empty.');
    }

    let cacheObject = {};

    function invokeCallback(...values) {
        
        if (cacheObject.hasOwnProperty(values)) {
            return cacheObject[values];
        }
        else{
            cacheObject[values] = callbackFunction(...values);
            return cacheObject[values];
        }
    }
    
    return invokeCallback;
}